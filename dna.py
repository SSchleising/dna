from pathlib import Path
import multiprocessing

def hist_gen(file):
    base_list = ["A", "C", "T", "G", "N", "a", "c", "t", "g", "n"]

    # for file in file_list:
    histogram = {}
    s = ""

    with open(file, 'r') as input_file:
        line = input_file.readline()
        for line in input_file.readlines():
            s += line

    print(file)

    for base in base_list:
        histogram[base] = s.count(base)

    print(histogram)

if __name__ == "__main__":
    # Set the data folder
    data_folder = 'data'

    p = Path(data_folder)

    # List all files in the data folder
    file_list = list(p.glob('*.fa'))

    with multiprocessing.Pool() as pool:
        pool.map(hist_gen, file_list)
