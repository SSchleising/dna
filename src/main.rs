use std::result::Result::Ok;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;
use std::collections::HashMap;
use glob::glob;
use std::thread;
use std::path::PathBuf;
use std::string;

fn main() {
    // Get the files which have extension .fa
    let entries = glob("data/*.fa");

    // Shadow entries with the actual value extracted from the result using match
    let entries = match entries {
        // If the result if Ok, extract the actual value from the result wrapper
        Ok(entries) => entries,
        // If the result is Err, print the error and quit, can do nothing more here
        Err(e) => {
            println!("{:?}", e);
            return},
    };

    // Create a vector to contain the thread handles to wait for termination
    let mut handles: Vec<thread::JoinHandle<()>> = Vec::new();

    // Loop through the files
    for entry in entries {
        // Shadow entry with the actual value extracted from the result using match
        let entry = match entry {
            Ok(entry) => entry,
            // In this case, if there is an error just break out of the loop and continue with other files
            Err(e) => {
                println!("{:?}", e);
                break},
        };

        // Display the filename
        println!("{}", entry.display());

        // Spawn the thread to read the files
        let handle = thread::spawn(move || {
            read_file(&entry);
        });

        // Add the new handle to the vector
        handles.push(handle)
    }

    // Wait for all the threads to signal completion
    for handle in handles {
        // Again use match to extract the result,
        // the Ok result doesn't seem to be anything useful so use _
        match handle.join() {
            Ok(_) => println!("Thread Terminated"),
            Err(e) => println!("{:?}", e),
        }
    }

    println!("Finished");
}

// Function to count the occurances of each base in a thread
fn read_file(path_buffer: &PathBuf) {
    // Open the file, it will be closed when file goes out of scope
    let file = File::open(path_buffer);

    // Same as before, use pattern macthing to extract the actual file handle
    let file = match file {
        Ok(file) => file,
        Err(e) => {
            println!("{:?}", e);
            return
        }
    };

    // Create a buffered reader
    let reader = BufReader::new(file);

    // Create a new hashmap to contain the counts
    let mut map: HashMap<&str, usize> = HashMap::new();

    let mut s: string::String = "".to_string();

    // Read each line in the file
    for line in reader.lines() {
        match line {
            Ok(line) => {
                if !line.starts_with(">") {
                    s += &line;
                }
            },
            Err(e) => println!("Error {}", e),
        }
    }

    // Add the counts to the hash map
    map.insert("A", s.matches("A").count());
    map.insert("C", s.matches("C").count());
    map.insert("T", s.matches("T").count());
    map.insert("G", s.matches("G").count());
    map.insert("N", s.matches("N").count());
    map.insert("a", s.matches("a").count());
    map.insert("c", s.matches("c").count());
    map.insert("t", s.matches("t").count());
    map.insert("g", s.matches("g").count());
    map.insert("n", s.matches("n").count());

    // Print the hash map
    println!("{:?}", map)
}
